DROP DATABASE IF EXISTS db_highscore_quallenspiel;
CREATE DATABASE db_highscore_quallenspiel;
USE db_highscore_quallenspiel;

CREATE TABLE scores (nr INTEGER AUTO_INCREMENT, vorname VARCHAR(30), versuche INTEGER, 
zeit INTEGER, PRIMARY KEY (nr)) ENGINE = INNODB;

INSERT INTO scores (vorname, versuche, zeit) VALUES ('Dario', 15, 12);

SELECT * FROM scores;