DROP DATABASE IF EXISTS db_freunde;
CREATE DATABASE db_freunde;
USE db_freunde;

CREATE TABLE freund (nr INTEGER AUTO_INCREMENT, vorname VARCHAR(30),
nachname VARCHAR(30), geburtsdatum DATE, PRIMARY KEY (nr))ENGINE = INNODB;

CREATE TABLE telefon (nr INTEGER, telefonnummer VARCHAR(30), freund_nr INTEGER,
PRIMARY KEY (nr), FOREIGN KEY (freund_nr) REFERENCES freund(nr))ENGINE = INNODB;

CREATE TABLE adresse (nr INTEGER, straße VARCHAR(30),
 plz VARCHAR(5), ort VARCHAR(30), freund_nr INTEGER,
 FOREIGN KEY (freund_nr) REFERENCES freund(nr),
 PRIMARY KEY (nr))ENGINE=INNODB;
 
 INSERT INTO freund (vorname, nachname, geburtsdatum)
VALUES ('Michel', 'Meisinger', '2001.09.23');

INSERT INTO freund (vorname, nachname, geburtsdatum)
VALUES ('Marvin', 'Stoll', '2000.09.23');

INSERT INTO freund (vorname, nachname, geburtsdatum)
VALUES ('Christopher', 'Kurz', '2000.11.27');

INSERT INTO freund (vorname, nachname, geburtsdatum)
VALUES ('Anna', 'Schwörer', '2001.06.11');

INSERT INTO freund (vorname, nachname, geburtsdatum)
VALUES ('Roland', 'Kain', '0000.12.24');


INSERT INTO telefon (nr, telefonnummer, freund_nr)
VALUES (1, '0800/858687', 1);

INSERT INTO telefon (nr, telefonnummer, freund_nr)
VALUES (2, '0800/757678', 2);

INSERT INTO telefon (nr, telefonnummer, freund_nr)
VALUES (3, '0800/626364', 3);

INSERT INTO telefon (nr, telefonnummer, freund_nr)
VALUES (4, '0800/515253', 4);

INSERT INTO telefon (nr, telefonnummer, freund_nr)
VALUES (5, '0900/272829', 5);





