CREATE DATABASE db_maschinen;
USE db_maschinen;

CREATE TABLE abteilung (abteilungsnr INTEGER, abteilungsname VARCHAR(30),

PRIMARY KEY (abteilungsnr))ENGINE = INNODB;

CREATE TABLE mitarbeiter (mitarbeiternr INTEGER, nachname VARCHAR(30), 
telefon VARCHAR(30),
abteilung_abteilungsnr INTEGER,

FOREIGN KEY (abteilung_abteilungsnr) REFERENCES abteilung(abteilungsnr),

PRIMARY KEY (mitarbeiternr))ENGINE=INNODB;

CREATE TABLE bauteil (bauteilnr INTEGER, preis DOUBLE, bauteilbezeichnung VARCHAR(50),
PRIMARY KEY (bauteilnr))ENGINE=INNODB;

CREATE TABLE maschinen (maschinennr INTEGER, anschaffungsdatum DATE,
funktion VARCHAR(50),
abteilung_abteilungsnr INTEGER,

FOREIGN KEY (abteilung_abteilungsnr) REFERENCES abteilung(abteilungsnr),
mitarbeiter_mitarbeiternr INTEGER,

FOREIGN KEY (mitarbeiter_mitarbeiternr) REFERENCES mitarbeiter(mitarbeiternr),
bauteil_bauteilnr INTEGER,

FOREIGN KEY (bauteil_bauteilnr) REFERENCES bauteil(bauteilnr),

PRIMARY KEY (maschinennr))ENGINE=INNODB;

CREATE TABLE kunde (kundennr INTEGER, kundenname VARCHAR(30), plz VARCHAR(5),
PRIMARY KEY (kundennr))ENGINE=INNODB;

CREATE TABLE v_kunde_bauteil (nr INTEGER,
kunde_kundennr INTEGER,
bauteil_bauteilnr INTEGER,

FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr),

FOREIGN KEY (bauteil_bauteilnr) REFERENCES bauteil(bauteilnr),

PRIMARY KEY (nr))ENGINE=INNODB;

