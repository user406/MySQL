DROP DATABASE IF EXISTS db_schule;
CREATE DATABASE db_schule;
USE db_schule;

CREATE TABLE schule (sname VARCHAR(30), email VARCHAR(30), adresse VARCHAR(30),
PRIMARY KEY (sname))ENGINE=INNODB;

CREATE TABLE klasse (kuerzel VARCHAR(10), bezeichnung VARCHAR(30), groesse INTEGER,
PRIMARY KEY (kuerzel))ENGINE=INNODB;