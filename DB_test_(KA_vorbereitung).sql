DROP DATABASE IF EXISTS db_test;
CREATE DATABASE db_test;
USE db_test;

CREATE TABLE artikel (artikelnr INTEGER AUTO_INCREMENT, bezeichnung VARCHAR(30),
preis DOUBLE,
PRIMARY KEY (artikelnr))ENGINE=INNODB;

CREATE TABLE kunde (kundennr INTEGER AUTO_INCREMENT, vorname VARCHAR(30),
nachname VARCHAR(30),
PRIMARY KEY (kundennr))ENGINE=INNODB;

CREATE TABLE adresse (nr INTEGER AUTO_INCREMENT, ort VARCHAR(30),
strasse VARCHAR(30), plz VARCHAR (5),
kunde_kundennr INTEGER,

FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr),
PRIMARY KEY (nr))ENGINE=INNODB;

CREATE TABLE bestellung (bestellnr INTEGER AUTO_INCREMENT, datum DATE,
kunde_kundennr INTEGER,
FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr),
PRIMARY KEY (bestellnr))ENGINE=INNODB;

CREATE TABLE artikelliste (nr INTEGER AUTO_INCREMENT, anzahl INTEGER,
bestellung_bestellnr INTEGER, artikel_artikelnr INTEGER,

FOREIGN KEY (artikel_artikelnr) REFERENCES artikel(artikelnr),
FOREIGN KEY (bestellung_bestellnr) REFERENCES bestellung(bestellnr),
PRIMARY KEY (nr))ENGINE=INNODB;

INSERT INTO kunde (vorname, nachname)
VALUES ('Klaus', 'Müller');

INSERT INTO kunde (vorname, nachname)
VALUES ('Peter', 'Maier');

INSERT INTO kunde (vorname, nachname)
VALUES ('Günter', 'Lauch');

INSERT INTO kunde (vorname, nachname)
VALUES ('Hans', 'Linde');

INSERT INTO artikel (bezeichnung, preis)
VALUES ('Schrauben', 2.50);

INSERT INTO artikel (bezeichnung, preis)
VALUES ('Naegel', 1.50);

INSERT INTO artikel (bezeichnung, preis)
VALUES ('Duebel', 0.50);

INSERT INTO artikel (bezeichnung, preis)
VALUES ('Bretter', 5.50);

INSERT INTO adresse (plz, ort, strasse, kunde_kundennr)
VALUES (79848, 'Bonndorf', 'Baumstrasse 5', 1);

INSERT INTO adresse (plz, ort, strasse, kunde_kundennr)
VALUES (79848, 'Bonndorf', 'Gartenstrasse 17', 2);

INSERT INTO adresse (plz, ort, strasse, kunde_kundennr)
VALUES (79848, 'Bonndorf', 'Lindenstrasse 4', 3);

INSERT INTO adresse (plz, ort, strasse, kunde_kundennr)
VALUES (79848, 'Bonndorf', 'Nussstrasse 56', 4);
