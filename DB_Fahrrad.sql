DROP DATABASE IF EXISTS db_fahrrad;
CREATE DATABASE db_fahrrad;
USE db_fahrrad;

CREATE TABLE kunde (vorname VARCHAR(30), nachname VARCHAR(30), kundennr INTEGER,
PRIMARY KEY (kundennr))ENGINE=INNODB;

CREATE TABLE hersteller (herstellername VARCHAR(30), email VARCHAR(30), herstellernr INTEGER,
PRIMARY KEY (herstellernr))ENGINE=INNODB;

CREATE TABLE fahrrad (anschaffungskosten DOUBLE, anschaffungsjahr DATE, fahrradnr INTEGER,
tagesmietpreis DOUBLE, bezeichnung VARCHAR(30),
hersteller_herstellernr INTEGER,

FOREIGN KEY (hersteller_herstellernr) REFERENCES hersteller(herstellernr),

PRIMARY KEY (fahrradnr))ENGINE=INNODB;

CREATE TABLE adresse (adressnr INTEGER, plz VARCHAR(5), ort VARCHAR(50), Strasse VARCHAR(50),
kunde_kundennr INTEGER,

FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr), 

PRIMARY KEY (adressnr))ENGINE=INNODB;

CREATE TABLE mietvertrag (vertragsnr INTEGER, mietbeginn DATE, mietende DATE, datum DATE,
kunde_kundennr INTEGER, fahrrad_fahrradnr INTEGER,

FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr),

FOREIGN KEY (fahrrad_fahrradnr) REFERENCES fahrrad(fahrradnr),

PRIMARY KEY (vertragsnr))ENGINE=INNODB;


