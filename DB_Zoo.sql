DROP DATABASE IF EXISTS db_zoo;
CREATE DATABASE db_zoo;
USE db_zoo;

CREATE TABLE pfleger (pflegernr INTEGER, telefon VARCHAR(30), nachname VARCHAR(30),
PRIMARY KEY (pflegernr))ENGINE = INNODB;

CREATE TABLE tierart (tierartnr INTEGER, tiername VARCHAR(30),
pfleger_pflegernr INTEGER,

FOREIGN KEY (pfleger_pflegernr) REFERENCES pfleger(pflegernr),

PRIMARY KEY (tierartnr))ENGINE = INNODB;

CREATE TABLE kaefig (position VARCHAR(30), kaefignr INTEGER,
PRIMARY KEY (kaefignr))ENGINE = INNODB;

CREATE TABLE v_tierart_kaefig (nr INTEGER, 
tierart_tierartnr INTEGER, 
kaefig_kaefignr INTEGER,

FOREIGN KEY (tierart_tierartnr) REFERENCES tierart(tierartnr),

FOREIGN KEY (kaefig_kaefignr) REFERENCES kaefig(kaefignr),

PRIMARY KEY (nr))ENGINE=INNODB;

INSERT INTO pfleger (pflegernr, telefon, nachname)
VALUES (1, '0100/00001', 'Maier');

INSERT INTO pfleger (pflegernr, telefon, nachname)
VALUES (2, '0100/00002', 'Lang');


INSERT INTO tierart (tierartnr, tiername, pfleger_pflegernr)
VALUES
(1, 'Löwe', 1),
(2, 'Tiger', 1),
(3, 'Panda', 2),
(4, 'Giraffe', 2);


INSERT INTO kaefig (position, kaefignr)
VALUES
('Gang A', 1),
('Gang B', 2);


INSERT INTO v_tierart_kaefig (nr, tierart_tierartnr, kaefig_kaefignr)
VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 2),
(4, 4, 2);