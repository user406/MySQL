CREATE DATABASE db_bestellung;
USE db_bestellung;

CREATE TABLE artikel (artikelnr INTEGER, bezeichnung VARCHAR(50), preis DOUBLE,
PRIMARY KEY (artikelnr))ENGINE=INNODB;

CREATE TABLE kunde (kundennr INTEGER, vorname VARCHAR(30), nachname VARCHAR(30),
PRIMARY KEY (kundennr))ENGINE=INNODB;

CREATE TABLE adresse (adressnr INTEGER, plz VARCHAR(5), ort VARCHAR(50), Strasse VARCHAR(50),
kunde_kundennr INTEGER,

FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr), 

PRIMARY KEY (adressnr))ENGINE=INNODB;

CREATE TABLE bestellung (bestellnr INTEGER, datum DATE,
kunde_kundennr INTEGER,

FOREIGN KEY (kunde_kundennr) REFERENCES kunde(kundennr), 

PRIMARY KEY (bestellnr))ENGINE=INNODB;

CREATE TABLE artikelliste (artikellistennr INTEGER, anzahl VARCHAR(30),
bestellung_bestellnr INTEGER,
artikel_artikelnr INTEGER,

FOREIGN KEY (bestellung_bestellnr) REFERENCES bestellung(bestellnr),

FOREIGN KEY (artikel_artikelnr) REFERENCES artikel(artikelnr),

PRIMARY KEY (artikellistennr))ENGINE=INNODB;
